USE ModernWays;
CREATE TABLE Metingen (
	Tijdstip DATETIME NOT NULL,
    Grootte SMALLINT UNSIGNED NOT NULL,
    Marge DECIMAL(3,2)
    );